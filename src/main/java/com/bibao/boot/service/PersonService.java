package com.bibao.boot.service;

import com.bibao.boot.model.Person;
import com.bibao.boot.model.PersonInfo;

public interface PersonService {
	public PersonInfo process(Person person);
}
