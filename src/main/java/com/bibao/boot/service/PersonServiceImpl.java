package com.bibao.boot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bibao.boot.dao.PersonDao;
import com.bibao.boot.model.Person;
import com.bibao.boot.model.PersonInfo;

@Service
public class PersonServiceImpl implements PersonService {
	@Autowired
	private PersonDao personDao;
	
	@Override
	public PersonInfo process(Person person) {
		personDao.save(person);
		PersonInfo info = new PersonInfo();
		info.setMessage("Hello " + person.getFullName() + ", welcome to Spring Boot MVC with Thymeleaf!");
		info.setPersonList(personDao.findAll());
		return info;
	}

}
